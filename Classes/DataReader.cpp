//
//  DataReader.cpp
//  LifeStorm2
//
//  Created by Nandi Wong on 10/3/15.
//
//

#include "DataReader.h"


unsigned char DataReader::ReadByte(unsigned char** data)
{
    return *(*data)++;
}

unsigned short DataReader::ReadShort(unsigned char** data)
{
    short value = *(*data)++;
    value += (*(*data)++) << 8;
    
    return value;
}

unsigned int DataReader::ReadInt(unsigned char** data)
{
    int value = *(*data)++;
    value += ((int)(*(*data)++) << 8);
    value += ((int)(*(*data)++) << 16);
    value += ((int)(*(*data)++) << 24);
    
    return value;
}
unsigned short DataReader::SeekShort(unsigned char** data , int position)
{
    short value = *(*data + position);
    value += (*(*data + 1 + position)) << 8;
    
    return value;
}

unsigned int DataReader::SeekInt(unsigned char** data , int position)
{
    int value = *(*data + position);
    value += ((int)(*(*data + 1 + position)) << 8);
    value += ((int)(*(*data + 2 + position)) << 16);
    value += ((int)(*(*data + 3 + position)) << 24);
    
    return value;
}