//
//  Adrn.hpp
//  LifeStorm2
//
//  Created by Nandi Wong on 29/12/2016.
//
//

#ifndef Adrn_hpp
#define Adrn_hpp

struct AdrnEntry
{
    int index;
    int position;
    int offsetX;
    int offsetY;
    int width;
    int height;
};

#include <stdio.h>
class Adrn {
private:
    std::map<int ,AdrnEntry> adrnMap;
    std::map<int ,int> tileIndexMap;
public:
    static Adrn* getInstance();
    void readFromStream();
    
    AdrnEntry getEntry(int position);
    int getTile(int position);
};
#endif /* Adrn_hpp */
