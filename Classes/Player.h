//
//  Player.h
//  LifeStorm2
//
//  Created by Nandi Wong on 2/1/2017.
//
//

#ifndef Player_hpp
#define Player_hpp

#include <stdio.h>
#include "PlayerRenderer.h"

USING_NS_CC;
class Player
{
private:
    bool isAnimating;
    
    Vec2 coordinate;
    PlayerRenderer* renderer;
    void playerPositionUpdated();
public:
    
    Player(int modelId);
    
    void setPosition(Vec2 coordinate);
    void moveToDirection(Direction direction);
    
    Vec2 getCoordinate() { return coordinate; };
    
    PlayerRenderer* getRenderer() { return renderer; }
};
#endif /* Player_hpp */
