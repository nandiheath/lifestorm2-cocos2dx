//
//  Spr.cpp
//  LifeStorm2
//
//  Created by Nandi Wong on 29/12/2016.
//
//

#include "Spr.h"
#include "DataReader.h"

USING_NS_CC;

static Spr* instance = 0;

Spr* Spr::getInstance()
{
    if (!instance)
    {
        instance = new Spr();
    }
    
    return instance;
}

SprEntry Spr::getEntry(int position)
{
    return sprMap[position];
}

void Spr::readFromStream()
{
    
    FileUtils* fileUtils = FileUtils::getInstance();
    Data data = fileUtils->getDataFromFile("res/spr_1.bin");
    unsigned char* buffer = data.getBytes();
    unsigned char* bufferStart = buffer;
    int size = 0;
    
    while (buffer < bufferStart + data.getSize())
    {
        SprEntry entry;
        
        entry.sprId = DataReader::ReadShort(&buffer);
        entry.unknown0 = DataReader::ReadShort(&buffer);
        entry.unknown1 = DataReader::ReadInt(&buffer);
        
        int feCount = 0;
        
        std::vector<int>* animationFrames = new std::vector<int>();
        
        while (feCount < 4)
        {
            int fe = DataReader::SeekShort(&buffer , 0);
            
            if (fe == 0xFFFE)
            {
                
                buffer += 2;
                feCount ++;
                continue;
            }
            if (feCount == 0)
            {
                // Reading the frames
                int adrnIndex = DataReader::ReadInt(&buffer);
                entry.adrnIds.push_back(adrnIndex);
                int unknown2 = DataReader::ReadInt(&buffer);
            }else if (feCount == 3)
            {
                
                int unknown3 = DataReader::ReadShort(&buffer);
                
                entry.unknown3.push_back(unknown3);
                //entry.unknown4.push_back(unknown4);
            }else if (feCount == 1)
            {
                // Reading the time of the frame maybe ?
                // The second segment seperated by 0xFFFE
                if (DataReader::SeekShort(&buffer , 0) == 0xFFFF)
                {
                    // seperator
                    buffer += 2;
                }else
                {
                    int unknown2 = DataReader::ReadShort(&buffer);
                    entry.unknown2.push_back(unknown2);
                }
                
            }else if (feCount == 2)
            {
                // should be a list of animations here
                if (DataReader::SeekShort(&buffer , 0) == 0xFFFF)
                {
                    // seperator
                    buffer += 2;
                    entry.animations.push_back(*animationFrames);
                    animationFrames = new std::vector<int>();
                }else
                {
                    int frame = DataReader::ReadInt(&buffer);
                    animationFrames->push_back(frame);
                }
            }
        }
        
        sprMap[entry.sprId] = entry;
        
        size ++;
        //CCLOG("[%x] --> %x" , buffer - bufferStart , entry.sprId);
    }
//    CCLOG("%d" , size);
//    CCLOG("%d" , sprMap[0x76D6].adrnIds.size());
//    CCLOG("%d" , sprMap[0x76D6].animations.size());
//    CCLOG("%d" , sprMap[0x76D6].unknown3.size());
//    CCLOG("%d" , sprMap[0x76D6].unknown5.size());
    
    
}