//
//  MapObjectRenderer.cpp
//  LifeStorm2
//
//  Created by Nandi Wong on 2/1/2017.
//
//

#include "MapObjectRenderer.h"
#include "Adrn.h"
#include "TextureCache.h"
#include "CoordinateConverter.h"

MapObjectRenderer::MapObjectRenderer(MapObject* mapObject)
{
    
    int adrnId = Adrn::getInstance()->getTile(mapObject->getMapObjectId());
    AdrnEntry adrnEntry = Adrn::getInstance()->getEntry(adrnId);
    Texture2D* texture = LS2TextureCache::getInstance()->getTextureForMap(mapObject->getMapObjectId());
    
    Vec2 coordinate = CoordinateConverter::ToScreenPoint(mapObject->getCoordinate());
    
    
    this->node = Sprite::createWithTexture(texture);
    this->node->setAnchorPoint(Vec2(0,0));
    
    this->node->setLocalZOrder(10000 - mapObject->getCoordinate().x + 10000 * mapObject->getCoordinate().y);
    
    // As the two coordinate system is different, LS2 (0,0) is top left , but cocos (0,0) is bottom left
    // the offset of the objects should be one grid
    this->node->setPosition(Vec2(coordinate.x + adrnEntry.offsetX , coordinate.y + ( - adrnEntry.offsetY + GRID_SIZE.height - adrnEntry.height )));
    this->node->retain();
}

MapObjectRenderer::~MapObjectRenderer()
{
    this->node->release();
}