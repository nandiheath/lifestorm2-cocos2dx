//
//  MapObjectModel.h
//  LifeStorm2
//
//  Created by Nandi Wong on 5/1/2017.
//
//

#ifndef MapObjectModel_hpp
#define MapObjectModel_hpp

#include <stdio.h>
USING_NS_CC;

class MapObjectModel {
private:
    int mapObjectId;
    int x;
    int y;
    
    
public:
    MapObjectModel(int mapObjectId , int x , int y);
    
    Vec2 getCoordinate() { return Vec2(x , y); };
    
    int getMapObjectId() { return mapObjectId; };
    
};

#endif /* MapObjectModel_hpp */
