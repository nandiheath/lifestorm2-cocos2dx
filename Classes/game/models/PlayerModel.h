//
//  PlayerModel.h
//  LifeStorm2
//
//  Created by Nandi Wong on 5/1/2017.
//
//

#ifndef PlayerModel_hpp
#define PlayerModel_hpp


#include <stdio.h>

USING_NS_CC;

class PlayerModel {
    
    int ls2ModelId;
    int x;
    int y;
    
public:
    
    PlayerModel(int modelId) { this->ls2ModelId = modelId; };
    
    void setCoornidate(Vec2 coord) { this->x = coord.x; this->y = coord.y; };
    Vec2 getCoordinate() { return Vec2(this->x , this->y); }
    
    int getModelId() { return ls2ModelId; };
};
#endif /* PlayerModel_hpp */
