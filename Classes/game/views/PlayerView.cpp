//
//  PlayerView.cpp
//  LifeStorm2
//
//  Created by Nandi Wong on 5/1/2017.
//
//

#include "PlayerView.h"
#include "Adrn.h"
#include "Spr.h"
#include "RealReader.h"
#include "CoordinateConverter.h"
USING_NS_CC;

PlayerView::PlayerView(PlayerModel* model)
: BaseView()
{
    SprEntry entry = Spr::getInstance()->getEntry(model->getModelId());
    for(std::vector<int>::iterator it = entry.adrnIds.begin(); it != entry.adrnIds.end(); ++it) {
        int adrnId = *it;
        AdrnEntry adrnEntry = Adrn::getInstance()->getEntry(adrnId);
        
        
        RealEntity* entity = RealReader::getInstance()->readData(adrnEntry.position);
        Texture2D* texture = new Texture2D;
        texture->initWithData(*(entity->getPixelBufferPtr()), entity->getDataSize(), Texture2D::PixelFormat::RGBA8888, entity->getWidth(), entity->getHeight(), Size(0 , 0));
        
        // Offset of X and Y is inverted
        SpriteFrame* frame = SpriteFrame::createWithTexture(texture, Rect(0, 0, entity->getWidth(), entity->getHeight()), false, Vec2(adrnEntry.offsetX,adrnEntry.offsetY + GRID_SIZE.height - adrnEntry.height), Size(entity->getWidth(), entity->getHeight()));
        frame->setAnchorPoint(Vec2(0,0));
        
        frames.pushBack(frame);
    }
    
    int indexCount = 0;
    for(std::vector<std::vector<int>>::iterator it = entry.animations.begin(); it != entry.animations.end(); ++it) {
        // Potential Leak
        std::vector<int> animationFrames = (*it);
        
        
        animationMap[indexCount ++] = animationFrames;
        
    }
    
    auto animations = Animation::createWithSpriteFrames(frames , 0.1);
    animations->setRestoreOriginalFrame(true);
    
    this->setSpriteFrame(*(frames.begin()));
    //this->runAction(RepeatForever::create(Animate::create(animations)));
}



Animate* PlayerView::getAnimation(int animationId)
{
    std::vector<int> frameIds = animationMap[animationId];
    
    Vector<SpriteFrame*> animationFrames;
    for(std::vector<int>::iterator it = frameIds.begin(); it != frameIds.end(); ++it)
    {
        if (*it > frames.size())
            continue;
        animationFrames.pushBack(frames.at(*it));
    }
    auto animations = Animation::createWithSpriteFrames(animationFrames , 0.03);
    animations->setRestoreOriginalFrame(true);
    
    return Animate::create(animations);
}


void PlayerView::playMoveAnimation(const std::function<void()>& function , Vec2 coordinate , DIRECTION direction)
{
    Vec2 newScreenPosition = CoordinateConverter::ToScreenPoint(coordinate);
    
    
    
    float duration = 0.8;
    // Create the actions
    FiniteTimeAction* actionMove =
    MoveTo::create( (float)duration , CoordinateConverter::ToScreenPoint(coordinate));
    FiniteTimeAction* actionMoveDone =
    CallFunc::create(function);
    
    this->runAction(Sequence::create( Spawn::create(Sequence::create(actionMove, actionMoveDone, NULL)
                                                    , getAnimation(3 * direction + 2) , NULL),
                                     NULL));
}

void PlayerView::setPositionFromModel(PlayerModel* model)
{
    this->setPosition(CoordinateConverter::ToScreenPoint(model->getCoordinate()));
}