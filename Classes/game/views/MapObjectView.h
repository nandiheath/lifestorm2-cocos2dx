//
//  MapObjectView.h
//  LifeStorm2
//
//  Created by Nandi Wong on 5/1/2017.
//
//

#ifndef MapObjectView_hpp
#define MapObjectView_hpp

#include <stdio.h>
#include "BaseView.h"
#include "MapObjectModel.h"

USING_NS_CC;

class MapObjectView : public BaseView{
    ~MapObjectView();
    
public:
    MapObjectView(MapObjectModel* mapObject);    
};

#endif /* MapObjectView_hpp */
