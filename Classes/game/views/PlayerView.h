//
//  PlayerView.h
//  LifeStorm2
//
//  Created by Nandi Wong on 5/1/2017.
//
//

#ifndef PlayerView_hpp
#define PlayerView_hpp

#include <stdio.h>
#include "BaseView.h"
#include "PlayerModel.h"
#include "Common.h"

class PlayerView : public BaseView
{
    
private:
    PlayerView();
    Vector<SpriteFrame*> frames;
    std::map<int ,std::vector<int>>  animationMap;
    
public:
    PlayerView(PlayerModel* model);
    
    Animate* getAnimation(int animationId);
    void playMoveAnimation(const std::function<void()>& function , Vec2 coordinate , DIRECTION direction);
    
    void setPositionFromModel(PlayerModel* model);
};
#endif /* PlayerView_hpp */
