//
//  MapObjectView.cpp
//  LifeStorm2
//
//  Created by Nandi Wong on 5/1/2017.
//
//

#include "MapObjectView.h"

#include "Adrn.h"
#include "TextureCache.h"
#include "CoordinateConverter.h"

MapObjectView::MapObjectView(MapObjectModel* mapObject)
:BaseView()
{
    
    int adrnId = Adrn::getInstance()->getTile(mapObject->getMapObjectId());
    AdrnEntry adrnEntry = Adrn::getInstance()->getEntry(adrnId);
    Texture2D* texture = LS2TextureCache::getInstance()->getTextureForMap(mapObject->getMapObjectId());
    
    Vec2 coordinate = CoordinateConverter::ToScreenPoint(mapObject->getCoordinate());
    
    
    this->initWithTexture(texture);
    this->setAnchorPoint(Vec2(0,0));
    
    
    
    // As the two coordinate system is different, LS2 (0,0) is top left , but cocos (0,0) is bottom left
    // the offset of the objects should be one grid
    this->setPosition(Vec2(coordinate.x + adrnEntry.offsetX , coordinate.y + ( - adrnEntry.offsetY + GRID_SIZE.height - adrnEntry.height )));
}

MapObjectView::~MapObjectView()
{    
}