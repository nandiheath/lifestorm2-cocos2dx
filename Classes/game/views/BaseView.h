//
//  BaseView.h
//  LifeStorm2
//
//  Created by Nandi Wong on 5/1/2017.
//
//

#ifndef BaseView_hpp
#define BaseView_hpp

#include <stdio.h>
USING_NS_CC;


class BaseView : public Sprite{
public:
    BaseView();
};
#endif /* BaseView_hpp */
