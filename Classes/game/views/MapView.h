//
//  MapView.h
//  LifeStorm2
//
//  Created by Nandi Wong on 5/1/2017.
//
//

#ifndef MapView_hpp
#define MapView_hpp

#include <stdio.h>
#include "BaseView.h"

#include "MapReader.h"

class MapController;

class MapView : public BaseView{
public:
    MapView();
    void initWithMapId(int mapId , LS2Map map , MapController* controller);
    
private:    
    
};
#endif /* MapView_hpp */
