//
//  Common.h
//  LifeStorm2
//
//  Created by Nandi Wong on 5/1/2017.
//
//

#ifndef Common_hpp
#define Common_hpp

#define GRID_SIZE Size(64, 47)

#define SCALE 1.0f

#include <stdio.h>

enum DIRECTION {
    DIR_DOWN = 0,
    DIR_DOWN_LEFT = 1,
    DIR_LEFT = 2,
    DIR_UP_LEFT = 3,
    DIR_UP = 4,
    DIR_UP_RIGHT = 5,
    DIR_RIGHT = 6,
    DIR_DOWN_RIGHT = 7,
} ;

#endif /* Common_hpp */
