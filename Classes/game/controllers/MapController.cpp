//
//  MapController.cpp
//  LifeStorm2
//
//  Created by Nandi Wong on 5/1/2017.
//
//

#include "MapController.h"
#include "BaseView.h"

MapView* MapController::getView(){
    return view;
}

MapController::MapController(int mapId)
{
    this->mapId = mapId;
}


#include "CoordinateConverter.h"
#include "Adrn.h"
#include "TextureCache.h"


void MapController::init()
{
    view = new MapView();
    LS2Map map = MapReader::getInstance()->readMap(this->mapId);
    this->width = map.width;
    this->height = map.height;
    view->initWithMapId(this->mapId , map , this);
    
}

MapController* MapController::createWithMapId(int mapId)
{
    MapController* controller = new MapController(mapId);
    controller->init();
    
    return controller;
}

int MapController::getZOrder(int x, int y)
{
    
    int size = (this->width * this->height);
    CCLOG("%d : %d = %d" , x , y , (size - x + this->width * y));
    return (size - x + this->width * y);
//    this->setLocalZOrder(10000 - mapObject->getCoordinate().x + 10000 * mapObject->getCoordinate().y);
}

void MapController::updateMapPosition(Vec2 pos)
{
    this->view->setPosition(pos);
}