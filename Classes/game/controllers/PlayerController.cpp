//
//  PlayerController.cpp
//  LifeStorm2
//
//  Created by Nandi Wong on 5/1/2017.
//
//

#include "PlayerController.h"
#include "PlayerModel.h"
#include "PlayerView.h"

PlayerController::PlayerController(int playerModelId)
{
    // TODO: contsructor init with player model?
    this->model = new PlayerModel(playerModelId);
    this->view = new PlayerView(this->model);
    
}

PlayerController* PlayerController::create(int playerModelId)
{
    PlayerController* controller = new PlayerController(playerModelId);
    
    return controller;
}

PlayerView* PlayerController::getView(){
    return view;
}

void PlayerController::setPosition(Vec2 coordinate)
{
    this->model->setCoornidate(coordinate);
    this->view->setPositionFromModel(this->model);
}

void PlayerController::moveToDirection(DIRECTION direction)
{
    // If doing animation , stop moving
    if (this->isAnimating)
        return;
    
    
    this->isAnimating = true;
    
    //this->getRenderer()->setPosition(CoordinateConverter::ToScreenPoint(coordinate));
    
    // Determine speed of the target
    
    Vec2 newCoordinate = this->model->getCoordinate();
    switch (direction)
    {
        case DIR_UP_LEFT:   newCoordinate.y -= 1;   break;
        case DIR_UP_RIGHT:   newCoordinate.x += 1;   break;
        case DIR_DOWN_LEFT:   newCoordinate.x -= 1;   break;
        case DIR_DOWN_RIGHT:   newCoordinate.y += 1;   break;
    }
    this->model->setCoornidate(newCoordinate);
    
    this->view->playMoveAnimation(CC_CALLBACK_0(PlayerController::onPlayerMoveAnimationEnd , this) , newCoordinate , direction);
}

void PlayerController::onPlayerMoveAnimationEnd()
{    
    this->isAnimating = false;
}