//
//  PlayerController.h
//  LifeStorm2
//
//  Created by Nandi Wong on 5/1/2017.
//
//

#ifndef PlayerController_hpp
#define PlayerController_hpp

#include <stdio.h>
#include "BaseController.h"
#include "Common.h"
#include "PlayerView.h"

class PlayerModel;
class PlayerController : public BaseController {
    
private:
    bool isAnimating;
    
    PlayerModel* model;
    PlayerView* view;
    
    PlayerController(int playerModelId);
    
    void onPlayerMoveAnimationEnd();
public:
    
    virtual PlayerView* getView();
    
    
    static PlayerController* create(int playerModelId);
    void setPosition(Vec2 coordinate);
    Vec2 getCoordinate(){ return this->model->getCoordinate(); };
    void moveToDirection(DIRECTION direction);
};
#endif /* PlayerController_hpp */
