//
//  BaseController.h
//  LifeStorm2
//
//  Created by Nandi Wong on 5/1/2017.
//
//

#ifndef BaseController_hpp
#define BaseController_hpp

#include <stdio.h>

#include "BaseView.h"

class BaseController {
    
    virtual BaseView* getView();
};
#endif /* BaseController_hpp */
