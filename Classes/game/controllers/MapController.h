//
//  MapController.h
//  LifeStorm2
//
//  Created by Nandi Wong on 5/1/2017.
//
//

#ifndef MapController_hpp
#define MapController_hpp

#include "MapModel.h"
#include "MapView.h"
#include "BaseController.h"

#include <stdio.h>
class MapController : public BaseController{
private:
    MapModel* model;
    MapView* view;
    
    int mapId;
    int width;
    int height;
    
    void init();
    MapController(int mapId);
public:
    static MapController* createWithMapId(int mapId);
    int getZOrder(int x , int y);
    virtual MapView* getView();
    
    void updateMapPosition(Vec2 pos);
};
#endif /* MapController_hpp */
