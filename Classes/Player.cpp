//
//  Player.cpp
//  LifeStorm2
//
//  Created by Nandi Wong on 2/1/2017.
//
//

#include "Player.h"
#include "CoordinateConverter.h"

Player::Player(int modelId){
    this->renderer = new PlayerRenderer(modelId);
}

void Player::setPosition(Vec2 coordinate)
{
    this->coordinate = coordinate;
    this->getRenderer()->setPosition(CoordinateConverter::ToScreenPoint(coordinate));
}

void Player::moveToDirection(Direction direction)
{
    // If doing animation , stop moving
    if (this->isAnimating)
        return;
    
    
    this->isAnimating = true;
    
    //this->getRenderer()->setPosition(CoordinateConverter::ToScreenPoint(coordinate));
    
    // Determine speed of the target
    int duration = 1;
    
    Vec2 newCoordinate = this->coordinate;
    switch (direction)
    {
        case UP_LEFT:   newCoordinate.y -= 1;   break;
        case UP_RIGHT:   newCoordinate.x += 1;   break;
        case DOWN_LEFT:   newCoordinate.x -= 1;   break;
        case DOWN_RIGHT:   newCoordinate.y += 1;   break;
    }
    this->coordinate = newCoordinate;
    
    this->getRenderer()->playMoveAnimation(CC_CALLBACK_0(Player::playerPositionUpdated , this) , newCoordinate , direction);
}

void Player::playerPositionUpdated()
{
    
    this->isAnimating = false;
}