//
//  CoordinateConverter.cpp
//  LifeStorm2
//
//  Created by Nandi Wong on 30/12/2016.
//
//

#include "CoordinateConverter.h"




Vec2 CoordinateConverter::ToScreenPoint(Vec2 gameCoordinate)
{
    return Vec2(
                (GRID_SIZE.width * gameCoordinate.y + GRID_SIZE.width * gameCoordinate.x) / 2,
                (GRID_SIZE.height * gameCoordinate.x - GRID_SIZE.height * gameCoordinate.y) / 2
    );
    
}

Vec2 CoordinateConverter::ToGameCoordinate(Vec2 screenPoint)
{
    int screenX = screenPoint.x + GRID_SIZE.width /2;
    return Vec2(
                     ((screenX * GRID_SIZE.height) + (screenPoint.y * GRID_SIZE.width)) / (GRID_SIZE.height * GRID_SIZE.width),
                     ((screenX * GRID_SIZE.height) - (screenPoint.y * GRID_SIZE.width)) / (GRID_SIZE.height * GRID_SIZE.width)
                     );
}
