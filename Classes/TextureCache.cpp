//
//  TextureCache.cpp
//  LifeStorm2
//
//  Created by Nandi Wong on 31/12/2016.
//
//

#include "TextureCache.h"
#include "Adrn.h"
#include "RealReader.h"

static LS2TextureCache* instance = 0;

LS2TextureCache* LS2TextureCache::getInstance()
{
    if (!instance)
    {
        instance = new LS2TextureCache();
    }
    return instance ;
}

Texture2D* LS2TextureCache::getTextureForMap(int mapObjectId)
{
    Texture2D* texture = textureMap[mapObjectId];
    if (!texture)
    {
        int adrnId = Adrn::getInstance()->getTile(mapObjectId);
        AdrnEntry adrnEntry = Adrn::getInstance()->getEntry(adrnId);
        RealEntity* entity = RealReader::getInstance()->readData(adrnEntry.position);
        texture = new Texture2D();
        texture->initWithData(*(entity->getPixelBufferPtr()), entity->getDataSize(), Texture2D::PixelFormat::RGBA8888, entity->getWidth(), entity->getHeight(), Size(entity->getWidth(), entity->getHeight()));
        
        texture->retain();
        
        textureMap[mapObjectId] = texture;
        
        CCLOG("texture map size : %d" , textureMap.size());
    }
    
    return texture;
}