//
//  DataReader.h
//  LifeStorm2
//
//  Created by Nandi Wong on 10/3/15.
//
//

#ifndef __LifeStorm2__DataReader__
#define __LifeStorm2__DataReader__

#include <stdio.h>


class DataReader
{
public:
    
    static unsigned char ReadByte(unsigned char** data);
    static unsigned short ReadShort(unsigned char** data);
    static unsigned int ReadInt(unsigned char** data);
    static unsigned short SeekShort(unsigned char** data , int position);
    static unsigned int SeekInt(unsigned char** data , int position);

};


#endif /* defined(__LifeStorm2__DataReader__) */
