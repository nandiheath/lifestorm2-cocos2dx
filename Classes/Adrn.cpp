//
//  Adrn.cpp
//  LifeStorm2
//
//  Created by Nandi Wong on 29/12/2016.
//
//

#include "Adrn.h"
#include "DataReader.h"

USING_NS_CC;

static Adrn* instance = 0;

Adrn* Adrn::getInstance()
{
    if (!instance)
    {
        instance = new Adrn();
    }
    
    return instance;
}

AdrnEntry Adrn::getEntry(int position)
{
    return adrnMap[position];
}

void Adrn::readFromStream()
{
    
    FileUtils* fileUtils = FileUtils::getInstance();
    Data data = fileUtils->getDataFromFile("res/adrn_1.bin");
    unsigned char* buffer = data.getBytes();
    
    const int blockSize = 76;
    int count = 0;
    for (int i =0 ; i < data.getSize()/blockSize; i ++)
    {
        
        AdrnEntry entry;
        
        int index = DataReader::ReadInt(&buffer);
        int position = DataReader::ReadInt(&buffer);
        int offsetX = DataReader::ReadInt(&buffer);
        int offsetY = DataReader::ReadInt(&buffer);
        
        int width = DataReader::ReadInt(&buffer);
        int height = DataReader::ReadInt(&buffer);
        
        // Unknown data
        buffer += 48;
        int imageno = DataReader::ReadInt(&buffer);
        if (imageno != 0)
        {
            tileIndexMap[imageno] = index;
        }
        
        // Some debug meesage
        if (position >= 0x0591D340 && count < 50)
        {
//            CCLOG("Index : %d , Position:%x , Offset:[%d ,%d] , Size:[%d , %d] , ImageNo : %d" ,
//                  index , position , offsetX, offsetY , height , width , imageno);
            count ++;
        }
        
        entry.index = index;
        entry.position = position;
        entry.offsetX = offsetX;
        entry.offsetY = offsetY;
        entry.width = width;
        entry.height = height;
        
        
        
        adrnMap[index] = entry;
    }
    
    //CCLOG("%d" , data.getSize()/blockSize);
}

int Adrn::getTile(int position)
{
    return tileIndexMap[position];
}