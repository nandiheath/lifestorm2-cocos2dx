//
//  GameWorldScene.cpp
//  LifeStorm2
//
//  Created by Nandi Wong on 5/1/2017.
//
//

#include "GameWorldScene.h"
#include "SimpleAudioEngine.h"
#include "DataReader.h"
#include "RealReader.h"
#include "Adrn.h"
#include "Spr.h"

#include "MapController.h"
#include "PlayerController.h"

Scene* GameWorldScene::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = GameWorldScene::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}


void GameWorldScene::update(float delta)
{
    Vec2 pos = playerController->getView()->getPosition();
    
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    
    mapController->updateMapPosition(Vec2(-pos.x * SCALE + visibleSize.width/2 , -pos.y  * SCALE + visibleSize.height/2 + 60));
    //SCALE
}


// on "init" you need to initialize your instance
bool GameWorldScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    
    // Audio
    auto audio = CocosDenshion::SimpleAudioEngine::getInstance();
    
    // set the background music and continuously play it.
    audio->playBackgroundMusic("res/bgm/ls2b_01.wav", true);
    
    
    // Make the update(dt) functional
    scheduleUpdate();
    
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    
    
    Adrn::getInstance()->readFromStream();
    Spr::getInstance()->readFromStream();
    
    // Init the keyboard listener
    auto eventListener = EventListenerKeyboard::create();
    
    Director::getInstance()->getOpenGLView()->setIMEKeyboardState(true);
    eventListener->onKeyReleased = CC_CALLBACK_2(GameWorldScene::onKeyReleased , this);
    
    this->_eventDispatcher->addEventListenerWithSceneGraphPriority(eventListener,this);
    
    
    
    // Init the controllers
    mapController = MapController::createWithMapId(1001);
    mapController->getView()->setScale(SCALE);
    
    this->addChild(mapController->getView());
    
    
    // TODO:
    int x = 41;
    int y = 53;
    
    playerController = PlayerController::create(0x76D6);
    playerController->setPosition(Vec2(x , y));
    mapController->getView()->addChild(playerController->getView());
    
    return true;
}


void GameWorldScene::onKeyReleased(EventKeyboard::KeyCode keyCode , Event* event)
{
    CCLOG("%d" , keyCode);
    switch ((int)keyCode)
    {
        case 29 : playerController->moveToDirection(DIR_DOWN_LEFT); break;
        case 28 : playerController->moveToDirection(DIR_UP_RIGHT); break;
        case 27 : playerController->moveToDirection(DIR_DOWN_RIGHT); break;
        case 26 : playerController->moveToDirection(DIR_UP_LEFT); break;
    }
    
    Vec2 coord = playerController->getCoordinate();
    playerController->getView()->setLocalZOrder(mapController->getZOrder(coord.x, coord.y) + 500);
    
    CCLOG("Depth : %d" , mapController->getZOrder(coord.x, coord.y));
    
}



