#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"
#include "DataReader.h"
#include "RealReader.h"
#include "Adrn.h"
#include "Spr.h"
#include "MapReader.h"
#include "CoordinateConverter.h"
#include "TextureCache.h"
#include "Player.h"
#include "MapRenderer.h"
#include "SimpleAudioEngine.h"
#define SCALE 2.0f
USING_NS_CC;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

void HelloWorld::testGetData()
{
    Adrn::getInstance()->readFromStream();
    Spr::getInstance()->readFromStream();
    
    LS2Map map = MapReader::getInstance()->readMap(1001);
    
    int count = 0;
    int i = 0;
    int position = 0x0591D340;
    //int position = 0x06779356;
    
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    
    MapRenderer* mapRenderer = new MapRenderer(1001);
    mapSprite = mapRenderer->getMapNode();
    /**
     Debug drawing one tile to test
     **/
    /*
     const int imageToTests[3] = {0xC9ED , 0xC9C3 , 0xC9D9};
    
    for (int i = 0; i < sizeof(imageToTests)/sizeof(int); i ++)
    {
        Vec2 coordinate = CoordinateConverter::ToScreenPoint(Vec2(0 , 0));
        int adrnId = imageToTests[i];
        AdrnEntry adrnEntry = Adrn::getInstance()->getEntry(adrnId);
        RealEntity* entity = RealReader::getInstance()->readData(adrnEntry.position);
        Texture2D* texture = new Texture2D;
        texture->initWithData(*(entity->getPixelBufferPtr()), entity->getDataSize(), Texture2D::PixelFormat::RGBA8888, entity->getWidth(), entity->getHeight(), Size(entity->getWidth(), entity->getHeight()));
        
        free(entity);
        Sprite* sprite = Sprite::createWithTexture(texture);
        sprite->setAnchorPoint(Vec2(0,0));
        //sprite->setPosition(Vec2(coordinate.x , coordinate.y));
        sprite->setPosition(Vec2(coordinate.x + adrnEntry.offsetX , coordinate.y + ( - adrnEntry.offsetY + GRID_SIZE.height - adrnEntry.height)));
        
        mapSprite->addChild(sprite);
    }
    //mapSprite->setPosition(100 , 0);
    mapSprite->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
     */
//    CCLOG("Tiles : %d , Objects : %d" ,  map.titles.size() , map.objects.size());
//    for (int y = 0; y < map.height; y++)
//    {
//        for (int x = 0; x < map.width ; x ++)
////      int x = 70; int y =  91;
////    for (int i = 0; i < 1 ; i ++)
//    
//        {
//            Vec2 coordinate = CoordinateConverter::ToScreenPoint(Vec2(y , x));
//            int mapTileId = map.titles[x + y * map.width];
//            if (mapTileId == 0)
//                continue;
//            int adrnId = Adrn::getInstance()->getTile(mapTileId);
//            AdrnEntry adrnEntry = Adrn::getInstance()->getEntry(adrnId);
//            Texture2D* texture = LS2TextureCache::getInstance()->getTextureForMap(mapTileId);
//            
//            Sprite* sprite = Sprite::createWithTexture(texture);
//            sprite->setAnchorPoint(Vec2(0,0));
//            sprite->setPosition(Vec2(coordinate.x + adrnEntry.offsetX , coordinate.y + ( - adrnEntry.offsetY + GRID_SIZE.height - adrnEntry.height)));
//            
//            sprite->setLocalZOrder(-mapTileId);
//            mapSprite->addChild(sprite);
//        }
//    }
//    
//    for (int y = 0; y < map.height; y++)
//    {
//        for (int x = map.width - 1; x > 0  ; x --)
//    //for (int i = 0; i < 1 ; i ++)
//    
//        {
//            
//            Vec2 coordinate = CoordinateConverter::ToScreenPoint(Vec2(y , x));
//            int mapTileId = map.objects[x + y * map.width];
//           
//            if (mapTileId == 0)
//                continue;
//            int adrnId = Adrn::getInstance()->getTile(mapTileId);
//            AdrnEntry adrnEntry = Adrn::getInstance()->getEntry(adrnId);
//            Texture2D* texture = LS2TextureCache::getInstance()->getTextureForMap(mapTileId);
//            
//            
//            Sprite* sprite = Sprite::createWithTexture(texture);
//            sprite->setAnchorPoint(Vec2(0,0));
//            
//            //free(entity);
//            
//            // As the two coordinate system is different, LS2 (0,0) is top left , but cocos (0,0) is bottom left
//            // the offset of the objects should be one grid
//            sprite->setPosition(Vec2(coordinate.x + adrnEntry.offsetX , coordinate.y + ( - adrnEntry.offsetY + GRID_SIZE.height - adrnEntry.height )));
//            //Vec2(coordinate.x - adrnEntry.offsetX , coordinate.y - adrnEntry.offsetY)
//            
//            // Use the mapTileId as the globalZOrder to use the auto-batching in order to reduce the glcalls
//            //sprite->setLocalZOrder(mapTileId);
//            mapSprite->addChild(sprite);
//        }
//    }
    
    
    mapSprite->setScale(SCALE);
    //mapSprite->setPosition( -1400 , 250);
    
    this->addChild(mapSprite);
    
    int x = 41;
    int y = 53;
//    Sprite* sprite = Sprite::create();
//    //sprite->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
//    sprite->setPosition(CoordinateConverter::ToScreenPoint(Vec2(x , y)));
//    //sprite->setVisible(false);
//    mapSprite->addChild(sprite);
    
    //    Sprite* sprite = Sprite::create();
    //    //sprite->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    //    sprite->setPosition(CoordinateConverter::ToScreenPoint(Vec2(x , y)));
    //    //sprite->setVisible(false);
    //    mapSprite->addChild(sprite);
    
    
    player = new Player(0x76D6);
    player->setPosition(Vec2(x , y));
    mapSprite->addChild(player->getRenderer());
    
    
    
    
    
    
    
//    Vector<SpriteFrame*> frames ;
//    
//    SprEntry entry = Spr::getInstance()->getEntry(0x76D6);
//    for(std::vector<int>::iterator it = entry.adrnIds.begin(); it != entry.adrnIds.end(); ++it) {
//        int adrnId = *it;
//        AdrnEntry adrnEntry = Adrn::getInstance()->getEntry(adrnId);
//        
//        
//        RealEntity* entity = RealReader::getInstance()->readData(adrnEntry.position);
//        Texture2D* texture = new Texture2D;
//        texture->initWithData(*(entity->getPixelBufferPtr()), entity->getDataSize(), Texture2D::PixelFormat::RGBA8888, entity->getWidth(), entity->getHeight(), Size(0 , 0));
//        
//        // Offset of X and Y is inverted
//        SpriteFrame* frame = SpriteFrame::createWithTexture(texture, Rect(0, 0, entity->getWidth(), entity->getHeight()), false, Vec2(adrnEntry.offsetX,-adrnEntry.offsetY), Size(entity->getWidth(), entity->getHeight()));
//        frame->setAnchorPoint(Vec2(0,0));
//        frames.pushBack(frame);
//    }
//    
//    auto animations = Animation::createWithSpriteFrames(frames , 0.1);
//    animations->setRestoreOriginalFrame(true);
//    
//    sprite->runAction(RepeatForever::create(Animate::create(animations)));
    
    
    
//    EventListenerCustom* listener = EventListenerCustom::create("my_event",
//                                                                CC_CALLBACK_0(YourSubclass::myVirtualEventMethod, this));
    auto eventListener = EventListenerKeyboard::create();
    
    Director::getInstance()->getOpenGLView()->setIMEKeyboardState(true);
    eventListener->onKeyReleased = CC_CALLBACK_2(HelloWorld::onKeyReleased , this);
    
//    eventListener->onKeyPressed = [=](EventKeyboard::KeyCode keyCode, Event* event){
//        // If a key already exists, do nothing as it will already have a time stamp
//        // Otherwise, set's the timestamp to now
//        CCLOG("%d" , keyCode);
//    };
//    eventListener->onKeyReleased = [=](EventKeyboard::KeyCode keyCode, Event* event){
//        // remove the key.  std::map.erase() doesn't care if the key doesnt exist
//        CCLOG("%d" , keyCode);
//        //x = ((int)keyCode == 30 ? 1 : 0);
//        //x += ((int)keyCode == 29 ? 1 : 0);
//        
////        sprite->setPosition(CoordinateConverter::ToScreenPoint(Vec2(x + (int)keyCode == 30 ? 1 : 0, y)));
//    };
    
    this->_eventDispatcher->addEventListenerWithSceneGraphPriority(eventListener,this);

    //while (ba < start + data.getSize())
        //for (int i = 0 ; i < 200 ; i ++)
    {
        
        
        
        
        
        
        
        
        
        //Image* image = new Image;
        //image->initWithImageData(imageData , re->GetDecompressedSize());
        
        count ++;
        
        
        // Output to file for testing
//        const std::string filePath = CCFileUtils::getInstance()->getWritablePath() + "/output.bmp";
//        printf("file size : %d" , fileSize);
//        printf("file path : %s" , filePath.c_str());
//        FILE *fp = fopen(filePath.c_str(), "wb");
//        fwrite(bitmap , sizeof(unsigned char) , fileSize , fp);
//        fclose(fp);
        
//        Texture2D* texture = new Texture2D;
//        //texture->initWithImage(image);
//        texture->initWithData(*(entity->getPixelBufferPtr()), entity->getDataSize(), Texture2D::PixelFormat::RGBA8888, entity->getWidth(), entity->getHeight(), Size(0 , 0));
//        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        //        if (flag1 == 0x50 && flag2 == 0x41)
        //            continue;
        
        
        //        CCLOG("Height = %zd" , width);
        //        CCLOG("Width = %zd" , height);
        //        CCLOG("Data Size = %d" , size);
        //        CCLOG("%d \n\n\n" , count ++ );
        
        
    }
}

void HelloWorld::onKeyReleased(EventKeyboard::KeyCode keyCode , Event* event)
{
    CCLOG("%d" , keyCode);
    Vec2 coordinate = player->getCoordinate();
    switch ((int)keyCode)
    {
        case 29 : player->moveToDirection(DOWN_LEFT); break;
        case 28 : player->moveToDirection(UP_RIGHT); break;
        case 27 : player->moveToDirection(DOWN_RIGHT); break;
        case 26 : player->moveToDirection(UP_LEFT); break;
    }
    
    
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    
    
    auto audio = CocosDenshion::SimpleAudioEngine::getInstance();
    
    // set the background music and continuously play it.
    audio->playBackgroundMusic("res/bgm/ls2b_01.wav", true);
    
    
    scheduleUpdate();
    
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    /////////////////////////////
    // 2. add a menu item with "X" image, which is clicked to quit the program
    //    you may modify it.

    // add a "close" icon to exit the progress. it's an autorelease object
    auto closeItem = MenuItemImage::create(
                                           "CloseNormal.png",
                                           "CloseSelected.png",
                                           CC_CALLBACK_1(HelloWorld::menuCloseCallback, this));
    
    closeItem->setPosition(Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width/2 ,
                                origin.y + closeItem->getContentSize().height/2));

    // create menu, it's an autorelease object
    auto menu = Menu::create(closeItem, NULL);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 1);

    /////////////////////////////
    // 3. add your codes below...

    // add a label shows "Hello World"
    // create and initialize a label
    
//    auto label = Label::createWithTTF("Hello World", "fonts/Marker Felt.ttf", 24);
//    
//    // position the label on the center of the screen
//    label->setPosition(Vec2(origin.x + visibleSize.width/2,
//                            origin.y + visibleSize.height - label->getContentSize().height));
//
//    // add the label as a child to this layer
//    this->addChild(label, 1);

    
    
    testGetData();
    
    return true;
}


void HelloWorld::menuCloseCallback(Ref* pSender)
{
    //Close the cocos2d-x game scene and quit the application
    Director::getInstance()->end();

    #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
    
    /*To navigate back to native iOS screen(if present) without quitting the application  ,do not use Director::getInstance()->end() and exit(0) as given above,instead trigger a custom event created in RootViewController.mm as below*/
    
    //EventCustom customEndEvent("game_scene_close_event");
    //_eventDispatcher->dispatchEvent(&customEndEvent);
    
    
}

void HelloWorld::update(float delta)
{
    Vec2 pos = player->getRenderer()->getPosition();
    
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    
    mapSprite->setPosition(-pos.x * SCALE + visibleSize.width/2 , -pos.y  * SCALE + visibleSize.height/2 + 60);
    //SCALE
}