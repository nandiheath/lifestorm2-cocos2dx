//
//  Spr.hpp
//  LifeStorm2
//
//  Created by Nandi Wong on 29/12/2016.
//
//

#ifndef Spr_hpp
#define Spr_hpp

USING_NS_CC;

struct SprEntry
{
    int sprId;
    int unknown0;
    int unknown1;
    std::vector<int> adrnIds;
    std::vector<int> unknown2;
    std::vector<int> unknown3;
    std::vector<int> unknown4;
    std::vector<int> unknown5;
    
    std::vector<std::vector<int>> animations;
};

#include <stdio.h>
class Spr {
private:
    std::map<int ,SprEntry> sprMap;
public:
    static Spr* getInstance();
    void readFromStream();
    
    SprEntry getEntry(int position);
    
};
#endif /* Spr_hpp */
