//
//  PlayerRenderer.h
//  LifeStorm2
//
//  Created by Nandi Wong on 2/1/2017.
//
//

#ifndef PlayerRenderer_hpp
#define PlayerRenderer_hpp

#include <stdio.h>
USING_NS_CC;
enum Direction {
    DOWN = 0,
    DOWN_LEFT = 1,
    LEFT = 2,
    UP_LEFT = 3,
    UP = 4,
    UP_RIGHT = 5,
    RIGHT = 6,
    DOWN_RIGHT = 7,
} ;


class PlayerRenderer: public Sprite
{
private:
    Vector<SpriteFrame*> frames;
    std::map<int ,std::vector<int>>  animationMap;
public:
    PlayerRenderer(unsigned int modelId);
    
    Animate* getAnimation(int animationId);
    void playMoveAnimation(const std::function<void()>& function , Vec2 coordinate , Direction direction);
    
    
};
#endif /* PlayerRenderer_hpp */
