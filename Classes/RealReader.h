//
//  RealReader.hpp
//  LifeStorm2
//
//  Created by Nandi Wong on 28/12/2016.
//
//

#ifndef RealReader_hpp
#define RealReader_hpp

#include <stdio.h>

class RealEntity
{
    int width;
    int height;
    
    ~RealEntity();
    
    unsigned char* data;
public:
    
    int WriteToPixels(unsigned char** buffer);
    unsigned char** getPixelBufferPtr();
    
    inline int getDataSize(){ return width * height * 4;}
    inline int getWidth(){ return width; }
    inline int getHeight(){ return height; }
    inline void setWidth(int value) { width = value; }
    inline void setHeight(int value) { height = value; }
};

class RealReader
{
public:
    static RealReader* getInstance();
    RealEntity* readData(int position);
    RealEntity* readNextData();
    void writePixelsToBuffer(unsigned char** buffer , int width , int height);
    
private:
    RealReader();
    unsigned char* realFilePtr;
    unsigned char* realCurrentPtr;
};



#endif /* RealReader_hpp */
