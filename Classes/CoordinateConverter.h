//
//  CoordinateConverter.hpp
//  LifeStorm2
//
//  Created by Nandi Wong on 30/12/2016.
//
//

#ifndef CoordinateConverter_hpp
#define CoordinateConverter_hpp

#include "Common.h"

USING_NS_CC;

#include <stdio.h>
class CoordinateConverter
{
public:
    static Vec2 ToScreenPoint(Vec2 gameCoordinate);
    static Vec2 ToGameCoordinate(Vec2 gameCoordinate);
};
#endif /* CoordinateConverter_hpp */
