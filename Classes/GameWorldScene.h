//
//  GameWorldScene.h
//  LifeStorm2
//
//  Created by Nandi Wong on 5/1/2017.
//
//

#ifndef GameWorldScene_hpp
#define GameWorldScene_hpp

#include <stdio.h>

class MapController;
class PlayerController;

class GameWorldScene : public cocos2d::Layer
{
private:
    
    MapController* mapController;
    PlayerController* playerController;
    
    virtual void update(float delta);
public:
    
    static cocos2d::Scene* createScene();
    virtual bool init();
    
    void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode , cocos2d::Event* event);
    
    // implement the "static create()" method manually
    CREATE_FUNC(GameWorldScene);
};
#endif /* GameWorldScene_hpp */
