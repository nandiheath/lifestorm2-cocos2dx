//
//  MapObjectRenderer.h
//  LifeStorm2
//
//  Created by Nandi Wong on 2/1/2017.
//
//

#ifndef MapObjectRenderer_hpp
#define MapObjectRenderer_hpp

#include "MapObject.h"

#include <stdio.h>
USING_NS_CC;

class MapObjectRenderer{
    Node* node;
    
    ~MapObjectRenderer();
    
public:
    MapObjectRenderer(MapObject* mapObject);
    Node* getNode() { return node; };
};
#endif /* MapObjectRenderer_hpp */
