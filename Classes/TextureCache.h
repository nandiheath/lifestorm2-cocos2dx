//
//  TextureCache.hpp
//  LifeStorm2
//
//  Created by Nandi Wong on 31/12/2016.
//
//

#ifndef TextureCache_hpp
#define TextureCache_hpp

#include <stdio.h>

USING_NS_CC;

class LS2TextureCache {
private:
    std::map<int ,Texture2D*> textureMap;
public:
    static LS2TextureCache* getInstance();
    
    Texture2D* getTextureForMap(int mapObjectId);
};

#endif /* TextureCache_hpp */
