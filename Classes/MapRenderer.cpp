//
//  MapRenderer.cpp
//  LifeStorm2
//
//  Created by Nandi Wong on 2/1/2017.
//
//

#include "MapObject.h"
#include "MapRenderer.h"
#include "MapReader.h"

#include "MapObjectRenderer.h"
#include "CoordinateConverter.h"
#include "Adrn.h"
#include "TextureCache.h"


MapRenderer::MapRenderer(int mapId)
{
    this->mapNode = new Sprite();
    LS2Map map = MapReader::getInstance()->readMap(mapId);
    
    // Draw the tiles onto the map node
    for (int y = 0; y < map.height; y++)
    {
        for (int x = 0; x < map.width ; x ++)
        {
            Vec2 coordinate = CoordinateConverter::ToScreenPoint(Vec2(x , y));
            int mapTileId = map.titles[x + y * map.width];
            if (mapTileId == 0)
                continue;
            int adrnId = Adrn::getInstance()->getTile(mapTileId);
            AdrnEntry adrnEntry = Adrn::getInstance()->getEntry(adrnId);
            Texture2D* texture = LS2TextureCache::getInstance()->getTextureForMap(mapTileId);
            
            Sprite* tileSprite = Sprite::createWithTexture(texture);
            tileSprite->setAnchorPoint(Vec2(0,0));
            tileSprite->setPosition(Vec2(coordinate.x + adrnEntry.offsetX , coordinate.y + ( - adrnEntry.offsetY + GRID_SIZE.height - adrnEntry.height)));
            
            tileSprite->setLocalZOrder(-mapTileId);
            this->mapNode->addChild(tileSprite);
        }
    }
    
    // Create the initial mapObjects
    for (int y = 0; y < map.height; y++)
    {
        for (int x = map.width - 1; x > 0  ; x --)
        {
            
            Vec2 coordinate = CoordinateConverter::ToScreenPoint(Vec2(x , y));
            int mapTileId = map.objects[x + y * map.width];
            
            if (mapTileId == 0)
                continue;
            
            MapObject* mapObject = new MapObject(mapTileId , x , y);
            MapObjectRenderer* mapObjectRenderer = new MapObjectRenderer(mapObject);
            
            this->mapNode->addChild(mapObjectRenderer->getNode());
        }
    }
}