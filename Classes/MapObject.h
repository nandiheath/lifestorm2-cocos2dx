//
//  MapObject.h
//  LifeStorm2
//
//  Created by Nandi Wong on 2/1/2017.
//
//

#ifndef MapObject_hpp
#define MapObject_hpp

#include <stdio.h>

USING_NS_CC;

class MapObject {
private:
    int mapObjectId;
    int x;
    int y;
    ~MapObject();
    
public:
    MapObject(int mapObjectId , int x , int y);
    
    Vec2 getCoordinate() { return Vec2(x , y); };
    
    int getMapObjectId() { return mapObjectId; };
    
};
#endif /* MapObject_hpp */
