//
//  MapReader.hpp
//  LifeStorm2
//
//  Created by Nandi Wong on 30/12/2016.
//
//

#ifndef MapReader_hpp
#define MapReader_hpp

#include <stdio.h>
USING_NS_CC;

struct LS2Map
{
    int width;
    int height;
    std::vector<int> titles;
    std::vector<int> objects;
};

#include <stdio.h>
class MapReader {
private:
    
public:
    static MapReader* getInstance();
    
    LS2Map readMap(int mapId);
    
};

#endif /* MapReader_hpp */
