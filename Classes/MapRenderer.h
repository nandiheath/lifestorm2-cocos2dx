//
//  MapRenderer.h
//  LifeStorm2
//
//  Created by Nandi Wong on 2/1/2017.
//
//

#ifndef MapRenderer_hpp
#define MapRenderer_hpp

#include <stdio.h>
#include "MapObject.h"
USING_NS_CC;

class MapRenderer
{
    MapObject* mapObject;
    Node* mapNode;
    
public:
    MapRenderer(int mapId);
    
    void initWithMapId(int mapId);
    Node* getMapNode() { return mapNode; }
    
};
#endif /* MapRenderer_hpp */
