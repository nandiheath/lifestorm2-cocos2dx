//
//  MapReader.cpp
//  LifeStorm2
//
//  Created by Nandi Wong on 30/12/2016.
//
//

#include "MapReader.h"
#include "DataReader.h"

USING_NS_CC;

static MapReader* instance = 0;
MapReader* MapReader::getInstance()
{
    if (!instance)
    {
        instance = new MapReader();
    }
    
    return instance;
}

LS2Map MapReader::readMap(int mapId)
{
    FileUtils* fileUtils = FileUtils::getInstance();
    std::string filename = "res/map/" + std::to_string(mapId) + ".dat";
    Data data = fileUtils->getDataFromFile(filename);
    
    unsigned char* buffer = data.getBytes();
    LS2Map map;
    map.width = DataReader::ReadInt(&buffer);
    map.height = DataReader::ReadInt(&buffer);
    int count = map.width * map.height;
    for (int i = 0; i < count; i ++)
    {
        map.titles.push_back(DataReader::ReadShort(&buffer));
        //DataReader::ReadShort(&buffer);
    }
    
    for (int i = 0; i < count; i ++)
    {
        map.objects.push_back(DataReader::ReadShort(&buffer));
        //DataReader::ReadShort(&buffer);
    }
    
    
    return map;
}

