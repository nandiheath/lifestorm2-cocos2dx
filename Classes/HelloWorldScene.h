#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__


#include "cocos2d.h"
#include "Player.h"

USING_NS_CC;

class HelloWorld : public cocos2d::Layer
{
private:
    Player* player;
    Node* mapSprite;
    
    virtual void update(float delta); 
public:
    static cocos2d::Scene* createScene();

    
    virtual bool init();
    
    // a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);
    
    void testGetData();
    
    void onKeyReleased(EventKeyboard::KeyCode keyCode , Event* event);
    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld);
};

#endif // __HELLOWORLD_SCENE_H__
